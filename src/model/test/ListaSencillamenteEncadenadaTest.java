package model.test;
import model.data_structures.ListaSencillamenteEncadenada;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

class ListaSencillamenteEncadenadaTest {

ListaSencillamenteEncadenada<String> linkedList;
	
   @BeforeEach
	public void setup() {
		linkedList = new ListaSencillamenteEncadenada<String>();
		linkedList.add(new String("Hakuna Matata"));
		linkedList.add(new String("Coco"));
	}

	/**
	 * getElement(T element) test
	 */
	@Test
	public void getElementByElement() {
		assertNull(linkedList.get(new String("Perozo")));
		assertNotNull(linkedList.get(new String("Coco")));
	}
	
	/**
	 * remove(T element) test
	 */
	@Test
	public void removeElement() {
		assertNotNull(linkedList.get(new String("Coco")));
		linkedList.delete(new String("Coco"));
		assertNull(linkedList.get(new String("Coco")));
	}
	
	/**
	 * Size Test
	 */
	@Test
	public void size() {
		assertEquals(linkedList.size(), 2);
		linkedList.delete(new String("Coco"));
		assertEquals(linkedList.size(), 1);
	}
	
	/**
	 * Get by index test
	 */
	@Test
	public void getByIndex() {
		assertEquals(linkedList.get(0).toString(),"Hakuna Matata");
		assertEquals(linkedList.get(1).toString(),"Coco");
		//Tries to get the element in position 2, should trigger IndexOutOfBoundsException
		try {
			assertEquals(linkedList.get(2).toString(),"Coco");
			assertFalse(true);
		} catch (IndexOutOfBoundsException e) {
			assertFalse(false);
		}
	}
	
	/**
	 * toArray() test
	 */
	@Test
	public void toArrayTest() {
		
	}
	
	/**
	 * getCurrent and next tests
	 */
	@Test
	public void getCurrent() {
		
	
		String first = linkedList.getCurrent();
		assertEquals("Hakuna Matata",first.toString());
		String in = linkedList.next();
		assertEquals("Coco",in.toString());
		String end = linkedList.getCurrent();
		assertEquals("Coco",end.toString());
	}
	
	/**
	 * set test
	 */
	@Test
	public void set() {
		assertEquals(linkedList.get(1).toString(),"Coco");
		linkedList.set(1,"Hola");
		assertEquals(linkedList.get(1).toString(),"Hola");
		//Tries to set the element in position 2, should trigger IndexOutOfBoundsException
		
		try {
			linkedList.set(2, "Chao");
			assertFalse(true);
		} catch (IndexOutOfBoundsException e) {
			assertFalse(false);
		}
	}

}
