package model.data_structures;

public class Node<T> {
	
	/**
	 * Constante de Serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Elemento almacenado en el nodo.
	 */
	protected T elemento;
	
	/**
	 * Siguiente nodo.
	 */
	protected Node<T> siguiente;
	
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenar� en el nodo. elemento != null
	 */
	public Node(T elemento)
	{
		this.elemento = elemento;
	}
	
	/**
	 * M�todo que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(Node<T> siguiente)
	{
		this.siguiente = siguiente;
	}
	
	
	
	/**
	 * M�todo que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public T darElemento()
	{
		return elemento;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenar� en el nodo.
	 */
	public void cambiarElemento(T elemento)
	{
		this.elemento = elemento;
	}
	
//	/**
//	 * M�todo que retorna el identificador del nodo.
//	 * Este identificador es el identificador del elemento que almacena.
//	 * @return Identificador del nodo (identificador del elemento almacenado).
//	 */
//	public String darIdentificador()
//	{
//		return elemento.darIdentificador();
//	}
	
	/**
	 * M�todo que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public Node<T> darSiguiente()
	{
		return siguiente;
	}

}
