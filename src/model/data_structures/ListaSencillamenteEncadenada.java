package model.data_structures;

import java.util.Collection;
import java.util.Iterator;

public class ListaSencillamenteEncadenada<T extends Comparable<T>> implements LinkedList<T>{


	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	protected int cantidadElementos;

	protected Node<T> actual;


	/**
	 * Primer nodo de la lista.
	 */
	protected Node<T> primerNodo;

	/**
	 * Construye la lista vac�a.
	 * <b>post: </b> Se ha inicializado el primer nodo en null
	 */
	public ListaSencillamenteEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
		actual = primerNodo;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaSencillamenteEncadenada(T nPrimero)
	{
		if(nPrimero == null)
			throw new NullPointerException("Se recibe un elemento nulo");
		this.primerNodo = new Node<T>(nPrimero);
		cantidadElementos = 1;
	}


	/**
	 * Indica el tama�o de la lista.
	 * @return La cantidad de nodos de la lista.
	 */
	public int size() 
	{
		return cantidadElementos;
	}


	/**
	 * Reemplaza el elemento de la posici�n por el elemento que llega por par�metro.
	 * @param index La posici�n en la que se desea reemplazar el elemento.
	 * @param element El nuevo elemento que se quiere poner en esa posici�n
	 * @return el m�todo que se ha retirado de esa posici�n.
	 * @throws IndexOutOfBoundsException si la posici�n es < 0 o la posici�n es >= size()
	 */
	public T set(int index, T element) throws IndexOutOfBoundsException 
	{
		// TODO Completar seg�n la documentaci�n
		if(index<0||index>=size())
		{
			throw new IndexOutOfBoundsException("El �ndice es inv�lido");
		}


		int posicion = 0;
		Node<T> nodo = primerNodo;
		boolean loEncontre = false;
		T elementoARetirar = null;

		//Si la lista est� vac�a
		if(nodo == null && index == 0)
		{
			primerNodo = new Node<T>(element);
		}


		while(nodo!=null&&!loEncontre)
		{
			if(index==posicion)
			{
				elementoARetirar = nodo.darElemento();
				nodo.cambiarElemento(element);
				loEncontre = true;
			}

			else{
				nodo = nodo.darSiguiente();
				posicion++;
			}
		}


		return elementoARetirar;

	}


	/**
	 * Borra de la lista todos los elementos en la colecci�nn que llega por par�metro
	 * @param coleccion la colecci�n de elmentos que se desea eliminar. coleccion != null
	 * @return true en caso que se elimine al menos un elemento o false en caso contrario
	 */


	public boolean isEmpty() 
	{
		// TODO Completar seg�n la documentaci�n
		if(primerNodo == null)
		{
			return true;
		}

		return false;
	}

	//	/**



	/**
	 * Agrega un elemento al final de la lista, actualiza el n�mero de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elem el elemento que se desea agregar.
	 * @return 
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */

	//	@Override
	//	public boolean add(Comparable<T> elemento) 
	//	{
	//		// TODO Completar seg�n la documentaci�n
	//	
	//	}

	@Override
	public boolean delete(T element) {
		// TODO Auto-generated method stub
		T a = element;
		boolean eliminado = false;
		Node<T> nodo = primerNodo;
		Node<T> anteriorNodo = null;




		//Si la lista est� vac�a
		if(primerNodo == null)
		{
			return false;
		}
		//Si hay un solo elemento
		if(primerNodo.darSiguiente()==null&& nodo.darElemento().compareTo( a )==0)
		{
			primerNodo = null;
			cantidadElementos--;
			return true;

		}

		//Si se quiere eliminar al primero
		if(primerNodo!=null&&nodo.darElemento().compareTo(a)==0&&primerNodo.darSiguiente()!=null)
		{
			primerNodo = primerNodo.darSiguiente();
			cantidadElementos--;
			return true;
		}


		while(nodo!=null && !eliminado)
		{


			//si el elemento actual tiene un siguiente
			if(a.compareTo(nodo.darElemento())==0&& nodo.darSiguiente()!=null)
			{
				anteriorNodo.cambiarSiguiente(nodo.darSiguiente());
				eliminado = true;
			}
			//si es el �ltimo elemento
			else if(a.compareTo(nodo.darElemento())==0&& nodo.darSiguiente()==null)
			{
				anteriorNodo.cambiarSiguiente(null);
				eliminado = true;

			}
			else
			{
				anteriorNodo = nodo;
				nodo = nodo.darSiguiente();


			}


		}


		if(eliminado)
		{
			cantidadElementos--;
			return true;
		}
		else
			return false;




	}

	@Override
	public T get(T element) {



		
		Node<T> nodo = primerNodo;
		boolean loEncontre = false;
		T elemento = null;

		//Si la lista est� vac�a



		while(nodo!=null&&!loEncontre)
		{

			if(nodo.darElemento().compareTo(element)==0)
			{
				elemento = nodo.darElemento();
				loEncontre = true;
			}
			else{
				nodo = nodo.darSiguiente();
				
			}
		}


		return elemento;
	}


	/**
	 * Devuelve el elemento de la posici�n dada
	 * @param pos la posici�n  buscada
	 * @return el elemento en la posici�n dada 
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */

	@Override
	public T get(int pos) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		// TODO Completar seg�n la documentaci�n
		if(pos<0||pos>=size())
		{
			throw new IndexOutOfBoundsException("El �ndice es inv�lido");
		}


		int posicion = 0;
		Node<T> nodo = primerNodo;
		boolean loEncontre = false;
		T elemento = null;

		//Si la lista est� vac�a



		while(nodo!=null&&!loEncontre)
		{
			if(pos==posicion)
			{
				elemento = nodo.darElemento();
				loEncontre = true;
			}

			else{
				nodo = nodo.darSiguiente();
				posicion++;
			}
		}


		return elemento;
	}

	/**
	 * Borra todos los elementos de la lista. Actualiza la cantidad de elementos en 0
	 * <b>post:</b> No hay elementos en la lista
	 */
	public void clear() 
	{
		// TODO Completar seg�n la documentaci�n
		primerNodo = null;
		cantidadElementos = 0;

	}

	@Override
	public void listing() {
		// TODO Auto-generated method stub
		actual = primerNodo;
	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return actual.darElemento();
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		actual = actual.darSiguiente();
		return actual==null?null:actual.darElemento();
	}



	@Override
	public boolean add(T elemento) {
		if(elemento==null)
		{
			throw new NullPointerException("El elemento es nulo");
		}

		if(primerNodo==null)
		{

			primerNodo = new Node<T>(elemento);
			cantidadElementos++;
			actual = primerNodo;
			return true;

		}
		T a = elemento;
		Node<T> nodo = primerNodo;
		Node<T> ultimo = primerNodo;
		boolean agregue = false;
		boolean existeIdRepetido = false;
		while(nodo!=null&&!existeIdRepetido)
		{
			if(nodo.darElemento().compareTo(a)==0)
			{
				existeIdRepetido  = true;
			}
			
		
			ultimo = nodo;
			nodo = nodo.darSiguiente();

		}

		if(existeIdRepetido)
		{
			agregue = false;
		}

		else
		{
			ultimo.cambiarSiguiente(new Node<T>(a));
			agregue = true;
			cantidadElementos++;

		}


		return agregue;
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return actual.darSiguiente()!=null;
	}




}
