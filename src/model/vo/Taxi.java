package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	
	String taxiId;
	String company;

	
	
	public Taxi(String taxiId, String company)
	{
		this.taxiId = taxiId;
		this.company = company;
		
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}
	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}
	
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		if(this.taxiId.compareToIgnoreCase(o.getTaxiId())==0)
		{
			return 0;
		}
		
		else if(this.taxiId.compareToIgnoreCase(o.getTaxiId())>1)
		{
			return 1;
		}
		
		else
		
			return -1;
		
		
		
		
	}	
}
