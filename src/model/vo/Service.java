package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	String tripId;
	String taxiId;
	double tripMiles;
	int tripSeconds;
	double tripTotal;
	int communityArea;
	
	public Service(String tripId, String taxiId, double tripMiles, int tripSeconds, double tripTotal, int communityArea) {
		// TODO Auto-generated constructor stub
		
		this.tripId = tripId;
		this.taxiId = taxiId;
		this.tripMiles = tripMiles;
		this.tripSeconds = tripSeconds;
		this.tripTotal = tripTotal;
		this.communityArea = communityArea;
		
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return tripId;
	}
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripseconds() {
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return tripTotal;
	}

	public int getCommunityArea() {
		return communityArea;
	}
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		if(tripId.compareTo(o.getTripId())==0)
			return 0;
		else if (tripId.compareTo(o.getTripId())>0)
			return 1;
		else
			return -1;
	}
	

}
