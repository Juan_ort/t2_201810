package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import javafx.scene.shape.Line;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.ListaSencillamenteEncadenada;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	
	//Creo la Lista de Servicios
	ListaSencillamenteEncadenada<Service> servicios = new ListaSencillamenteEncadenada<Service>();
	ListaSencillamenteEncadenada<Taxi> taxis = new ListaSencillamenteEncadenada<Taxi>();

	
	
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub

		JsonParser parser = new JsonParser();
		
		try {
			
			
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));
			
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				
				JsonObject obj= (JsonObject) arr.get(i);
				
				/* Obtener la propiedad taxiId de un servicio (String)*/
				String taxiId = "";
				if ( obj.get("taxi_id") != null )
				{ taxiId = obj.get("taxi_id").getAsString(); }
				
				
				/* Obtener la propiedad tripId de un servicio (String)*/
				String tripId = "NaN";
				if ( obj.get("trip_id") != null )
				{ tripId = obj.get("trip_id").getAsString(); }
				
				
				/* Obtener la propiedad tripMiles de un servicio (double)*/
				double tripMiles=0;
				if ( obj.get("trip_miles") != null )
				{ tripMiles = obj.get("trip_miles").getAsDouble(); }
				
				/* Obtener la propiedad tripSeconds de un servicio (int)*/
				int tripSeconds=0;
				if ( obj.get("trip_seconds") != null )
				{ tripSeconds = obj.get("trip_seconds").getAsInt(); }
				
				/* Obtener la propiedad tripTotal de un servicio (Double)*/
				double tripTotal=0;
				if ( obj.get("trip_total") != null )
				{ tripTotal= obj.get("trip_total").getAsDouble(); }
				
				/* Obtener la propiedad Community Area de un servicio (String)*/
				int communityArea=0;
				if ( obj.get("dropoff_community_area") != null )
				{ communityArea = obj.get("dropoff_community_area").getAsInt(); }
				
				/* Obtener la propiedad company de un servicio (String)*/
				String company = "NaN";
				if ( obj.get("company") != null )
				{ company = obj.get("company").getAsString(); }
				
				
				
				
				Service actual = new Service(tripId, taxiId, tripMiles, tripSeconds, tripTotal, communityArea);
				
				Taxi taxiActual = new Taxi(taxiId, company);
				taxis.add(taxiActual);
				
				servicios.add(actual);
			
			
				
			
			}
			
			System.out.println("La cantidad de servicios es " + servicios.size() + ". Mientras que la cantidad de taxis es " + taxis.size()+"." );
		}
		
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		ListaSencillamenteEncadenada<Taxi> taxisCompany = new ListaSencillamenteEncadenada<Taxi>();
		for (int i = 0; i<taxis.size();i++) {
			Taxi taxi = taxis.get(i);
			if(taxi.getCompany().equals(company))
			{
				taxisCompany.add(taxi);

			}
		}
		System.out.println(taxisCompany.size());
		return taxisCompany;
		
		
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
	
		ListaSencillamenteEncadenada<Service> nuevaLista = new ListaSencillamenteEncadenada<Service>();
		
		for(int i = 0 ; i< servicios.size(); i++)
		{

			int codigo = servicios.get(i).getCommunityArea();
			if(codigo==communityArea)
			{
				nuevaLista.add(servicios.get(i));
				
			}
		}
		

	
		return nuevaLista;
	}

	/**
	 * Ejemplo basico de lectura de archivo JSON con servicios de taxi
	 * Sitio web de consulta: https://data.cityofchicago.org/Transportation/Taxi-Trips/wrvz-psew
	 * @author Fernando De la Rosa 
	 */
	public void mostrarObjetosTaxiTrips()
	{
		JsonParser parser = new JsonParser();
		
		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";
			
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));
			
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				/* Mostrar un JsonObject (Servicio taxi) */
				System.out.println("------------------------------------------------------------------------------------------------");
				System.out.println(obj);
				
				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }
				
				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }
				
				JsonObject dropoff_localization_obj = null; 
				
				System.out.println("(Lon= "+dropoff_centroid_longitude+ ", Lat= "+dropoff_centroid_latitude +") (Datos String)" );

				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();
				
				  /* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				  JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();
				  
				  /* Obtener cada coordenada del JsonArray como Double */
				  double longitude = dropoff_localization_arr.get(0).getAsDouble();
				  double latitude = dropoff_localization_arr.get(1).getAsDouble();
				  System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");
				}
				else
				{
					System.out.println( "[Lon: NaN, Lat: NaN ]");
				}
			}
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}

}
